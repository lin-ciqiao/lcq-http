package com.lcq.httplib;

public enum ErrorEnum {
    SUCCESS(0, "success"),
    NET_FAIL(1000, "net fail"),
    PARSE_FAIL(1001, "parse fail"),
    DECRYPT_FAIL(1002, "decrypt fail"),
    BODY_NULL(1003, "null response body"),
    IO_EXCEPTION(1005, "io exception"),
    DATA_PARSER_NULL(1006, "data parser is null");

    private final int code;
    private final String msg;

    ErrorEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
