package com.lcq.httplib.callback;

import java.lang.reflect.Type;

public interface DataCallback<T> extends Callback<T> {
    Type getDataType();
}
