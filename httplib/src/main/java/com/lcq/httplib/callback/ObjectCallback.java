package com.lcq.httplib.callback;

import java.lang.reflect.Type;

public abstract class ObjectCallback implements DataCallback<Object> {
    @Override
    public Type getDataType() {
        return Object.class;
    }
}
