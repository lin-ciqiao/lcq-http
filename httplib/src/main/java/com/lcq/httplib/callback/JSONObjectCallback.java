package com.lcq.httplib.callback;

import org.json.JSONObject;

import java.lang.reflect.Type;

public abstract class JSONObjectCallback implements DataCallback<JSONObject> {
    @Override
    public Type getDataType() {
        return JSONObject.class;
    }
}
