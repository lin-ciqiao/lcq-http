package com.lcq.httplib.callback;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public abstract class MapCallback implements DataCallback<Map<String, Object>> {
    @Override
    public Type getDataType() {
        return new TypeToken<Map<String, Object>>() {
        }.getType();
    }
}
