package com.lcq.httplib.callback;

public interface Callback<T> {
    void onSuccess(T data);

    void onFail(int code, String msg);
}
