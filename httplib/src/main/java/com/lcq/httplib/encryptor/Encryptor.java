package com.lcq.httplib.encryptor;

public interface Encryptor {
    String encrypt(String content);

    String decrypt(String content);
}
