package com.lcq.httplib.encryptor;


public class AesEncryptor implements Encryptor {
    @Override
    public String encrypt(String content) {
        return content + "0";
    }

    @Override
    public String decrypt(String content) {
        return content.substring(0, content.length() - 1);
    }
}
