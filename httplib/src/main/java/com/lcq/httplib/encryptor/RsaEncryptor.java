package com.lcq.httplib.encryptor;

public class RsaEncryptor implements Encryptor {

    public RsaEncryptor() {
    }

    @Override
    public String encrypt(String content) {
        return content + "1";
    }

    @Override
    public String decrypt(String content) {
        return content.substring(0, content.length() - 1);
    }
}
