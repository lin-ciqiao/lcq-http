package com.lcq.httplib.parser;

import com.lcq.httplib.callback.DataCallback;

public interface DataParser<T> {
    void parse(String content, DataCallback<T> callback);
}
