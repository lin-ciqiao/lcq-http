package com.lcq.httplib.parser;

import com.google.gson.Gson;
import com.lcq.httplib.callback.DataCallback;

import java.util.Map;

public class WeatherParser implements DataParser<Map<String, Object>> {
    @Override
    public void parse(String content, DataCallback<Map<String, Object>> callback) {
        WeatherInfo weatherInfo = new Gson().fromJson(content, WeatherInfo.class);
        callback.onSuccess(weatherInfo.getWeatherinfo());
    }
}
