package com.lcq.httplib.client;

import com.lcq.httplib.client.adapter.ICallAdapter;

public abstract class AbsClient<T> implements ICallAdapter {
    protected final ClientParams<T> clientParams;
    protected ICallAdapter callService;

    public AbsClient(ClientParams<T> clientParams) {
        this.clientParams = clientParams;
    }

    public void call() {
        if (callService == null) {
            callService = createCallService();
        }
        callService.call();
    }

    public void cancel() {
        if (callService == null) {
            return;
        }

        callService.cancel();
    }

    public ClientParams<T> getCallParams() {
        return clientParams;
    }

    protected abstract ICallAdapter createCallService();
}
