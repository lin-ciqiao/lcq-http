package com.lcq.httplib.client;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.lcq.httplib.client.adapter.ICallAdapter;
import com.lcq.httplib.client.adapter.InputStreamCallAdapter;
import com.lcq.httplib.client.adapter.RequestParams;
import com.lcq.httplib.callback.Callback;

import java.io.InputStream;

public class BitmapClient extends AbsClient<Bitmap> {
    public BitmapClient(ClientParams<Bitmap> clientParams) {
        super(clientParams);
    }

    @Override
    protected ICallAdapter createCallService() {

        RequestParams.Builder<InputStream> builder = new RequestParams.Builder<InputStream>()
                .url(clientParams.getUrl())
                .async(clientParams.isAsync())
                .headers(clientParams.getHeaders())
                .callBack(new Callback<InputStream>() {
                    @Override
                    public void onSuccess(InputStream data) {
                        Bitmap bitmap = BitmapFactory.decodeStream(data);
                        clientParams.getCallback().onSuccess(bitmap);
                    }

                    @Override
                    public void onFail(int code, String msg) {
                        clientParams.getCallback().onFail(code, msg);
                    }
                });
        return new InputStreamCallAdapter(builder.build());
    }
}
