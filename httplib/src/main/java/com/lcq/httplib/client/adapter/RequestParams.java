package com.lcq.httplib.client.adapter;

import android.util.Log;

import com.lcq.httplib.callback.Callback;

import java.util.Map;

import okhttp3.RequestBody;

public class RequestParams<T> {
    private final String url;
    private final Map<String, String> headers;
    private final RequestBody requestBody;
    private final boolean async;
    private Callback<T> callback;

    private RequestParams(Builder<T> builder) {
        url = builder.url;
        headers = builder.headers;
        requestBody = builder.requestBody;
        async = builder.async;
        callback = builder.callBack;
    }

    public String getUrl() {
        return url;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }


    public boolean isAsync() {
        return async;
    }

    public Callback<T> getCallback() {
        if (callback == null) {
            callback = new Callback<T>() {
                @Override
                public void onSuccess(T data) {
                    Log.d("http", "onSuccess,no callback");
                }

                @Override
                public void onFail(int code, String msg) {
                    Log.d("http", "onFail,no callback");
                }
            };
        }
        return callback;
    }

    public static final class Builder<T> {
        private String url;
        private Map<String, String> headers;
        private RequestBody requestBody;
        private boolean async;
        private Callback<T> callBack;

        public Builder() {
        }

        public Builder<T> url(String val) {
            url = val;
            return this;
        }

        public Builder<T> headers(Map<String, String> val) {
            headers = val;
            return this;
        }

        public Builder<T> requestBody(RequestBody val) {
            requestBody = val;
            return this;
        }

        public Builder<T> async(boolean val) {
            async = val;
            return this;
        }

        public Builder<T> callBack(Callback<T> val) {
            callBack = val;
            return this;
        }

        public RequestParams<T> build() {
            return new RequestParams<>(this);
        }
    }
}
