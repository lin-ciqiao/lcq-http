package com.lcq.httplib.client.adapter;

import com.lcq.httplib.ErrorEnum;

import java.io.InputStream;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;

public class InputStreamCallAdapter extends AbsCallAdapter<InputStream> {
    public InputStreamCallAdapter(RequestParams<InputStream> requestParams) {
        super(requestParams);
    }

    @Override
    public Call createOkHttpCall() {
        return new OkHttpClient().newCall(new Request.Builder()
                .url(requestParams.getUrl())
                .build());
    }

    @Override
    protected void handleSuccessBody(ResponseBody responseBody) {
        try {
            InputStream inputStream = responseBody.byteStream();
            requestParams.getCallback().onSuccess(inputStream);
            responseBody.close();
        } catch (Exception e) {
            requestParams.getCallback().onFail(ErrorEnum.PARSE_FAIL.getCode(), ErrorEnum.PARSE_FAIL.getMsg());
        }
    }
}
