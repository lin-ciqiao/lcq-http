package com.lcq.httplib.client;

import com.google.gson.Gson;
import com.lcq.httplib.encryptor.Encryptor;
import com.lcq.httplib.parser.DataParser;
import com.lcq.httplib.parser.WeatherParser;

import java.util.Map;

public class WeatherClient extends StringClient<Map<String, Object>> {
    public WeatherClient(ClientParams<Map<String, Object>> clientParams) {
        super(clientParams);
    }

    @Override
    protected String postParamToText(Object param) {
        if (param == null) return null;//自己根据服务端协议，将param转换为需要的post文本
        return new Gson().toJson(param);
    }

    @Override
    protected Encryptor encryptor() {
        return null;
    }

    @Override
    protected DataParser<Map<String, Object>> dataParser() {
        return new WeatherParser();
    }
}
