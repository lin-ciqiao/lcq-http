package com.lcq.httplib.client;

import com.google.gson.Gson;
import com.lcq.httplib.encryptor.Encryptor;
import com.lcq.httplib.parser.CommonDataParser;
import com.lcq.httplib.parser.DataParser;

public class DefaultStringClient<T> extends StringClient<T> {
    public DefaultStringClient(ClientParams<T> clientParams) {
        super(clientParams);
    }

    @Override
    public String postParamToText(Object param) {
        if (param == null) {
            return null;
        }

        if (param instanceof String) {
            return param.toString();
        }

        return new Gson().toJson(param);
    }

    @Override
    protected Encryptor encryptor() {
        return null;
    }

    @Override
    public DataParser<T> dataParser() {
        return new CommonDataParser<>();
    }
}
