package com.lcq.httplib.client;

import com.lcq.httplib.encryptor.Encryptor;
import com.lcq.httplib.encryptor.RsaEncryptor;

public class RsaClient<T> extends DefaultStringClient<T> {

    public RsaClient(ClientParams<T> clientParams) {
        super(clientParams);
    }

    @Override
    protected Encryptor encryptor() {
        return new RsaEncryptor();
    }
}
