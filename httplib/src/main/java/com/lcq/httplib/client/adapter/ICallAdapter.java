package com.lcq.httplib.client.adapter;

public interface ICallAdapter {
    void call();

    void cancel();
}
