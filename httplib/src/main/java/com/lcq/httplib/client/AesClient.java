package com.lcq.httplib.client;


import com.lcq.httplib.encryptor.AesEncryptor;
import com.lcq.httplib.encryptor.Encryptor;

public class AesClient<T> extends DefaultStringClient<T> {

    public AesClient(ClientParams<T> clientParams) {
        super(clientParams);
    }

    @Override
    protected Encryptor encryptor() {
        return new AesEncryptor();
    }
}
