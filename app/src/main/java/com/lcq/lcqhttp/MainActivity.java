package com.lcq.lcqhttp;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.lcq.httplib.callback.MapCallback;
import com.lcq.httplib.client.ClientParams;
import com.lcq.httplib.client.WeatherClient;

import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void testClient(View view) {
        new WeatherClient(new ClientParams.Builder<Map<String, Object>>()
                .url("http://www.weather.com.cn/data/cityinfo/101010100.html")
                .callback(new MapCallback() {
                    @Override
                    public void onSuccess(Map<String, Object> data) {
                        StringBuilder sb = new StringBuilder("天气信息：");
                        for (Map.Entry<String, Object> entry : data.entrySet()) {
                            sb.append(entry.getKey()).append(" = ").append(entry.getValue()).append("\n");
                            Log.d("http", "entry:" + entry.getKey() + "=" + entry.getValue());
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, sb.toString(), Toast.LENGTH_LONG).show();
                            }
                        });

                    }

                    @Override
                    public void onFail(int code, String msg) {
                        Log.d("http", "onFail.code:" + code + "msg:" + msg);
                    }
                })
                .build())
                .call();
    }
}